function [ x ] = solve_quadratic( y,lambda )
% Input y
% Output optimal x
% Procedure: First we work with the abslute value of y. Then, we find a
% permutation that sorts y in decreasing order. We solve the quadratic
% program for x. we change the sign of each x to match that of y.
n=length(y);
y_abs=abs(y);
[~,index]=sort(abs(y_abs),'descend');
y_abs_sorted=y_abs(index);
Permute=zeros(n,n);
for i=1:n
    Permute(i,index(i))=1;
end
% now we have y_abs_sorted=  Permute * y_abs; y_abs=Permute'*y_abs_sorted;

% Construct linear constraint matrix
A=zeros(n+1,n);
for i=1:n-1
    A(i,i)=1;  A(i,i+1)=-1;
end
A(n,1)=1;A(n+1,n)=1;
% Quadratic program is .5 x'x + (lambda-y)' x
x = quadprog(eye(n),lambda-y_abs_sorted,-A,zeros(n+1,1),[],[],zeros(n,0),[]);
x= Permute'*x; % optimal solution before permutation
x= sign(y).* x; % restoring the sign of x
end