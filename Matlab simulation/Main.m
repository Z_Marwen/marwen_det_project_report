%% Matlab code to generate Figure 8 of sorted Lasso paper
% Author: Marwen Zorgui
clear
close all
p=1000;
n=p;
rho=0.5 ;
Sigma=rho*ones(n,n)+(1-rho)*eye(n,n);
Whitening_matrix=Sigma^(-1/2);
mu=sqrt(2*log(p));
q1=0.1;
q2=0.05;
N_average=5;
c=norm(Whitening_matrix(:,1));
pseudo_inverse=pinv(Whitening_matrix);
% Ordered lasso coefficients
lambda_q1=zeros(n,1);
lambda_q2=zeros(n,1);
for i=1:n
    lambda_q1(i)= norminv(1-q1*i /(2*p),0,1);
    lambda_q2(i)= norminv(1-q2*i /(2*p),0,1);
end
%% Main code
k_axis=[0:3:20 20:10:80];

% Initialize vectors that store the average perforamnce for each k (we define a variable for each value of q)
FDP_HBq_std_dev1=zeros(1,length(k_axis));FDP_HBq_std_dev2=zeros(1,length(k_axis));
FDR_HBq_aver1=zeros(1,length(k_axis));FDR_HBq_aver2=zeros(1,length(k_axis));
beta_HBq_aver1=zeros(1,length(k_axis));beta_HBq_aver2=zeros(1,length(k_axis));

FDR_L1_aver1=zeros(1,length(k_axis));FDR_L1_aver2=zeros(1,length(k_axis));
FDP_L1_std_dev1=zeros(1,length(k_axis));FDP_L1_std_dev2=zeros(1,length(k_axis));
beta_L1_aver1=zeros(1,length(k_axis));beta_L1_aver2=zeros(1,length(k_axis));

%% Loop
parfor j=1:length(k_axis)
    j
    k=k_axis(j);
    mean=zeros(p,1);
    mean(1:k)=mu;
    % Initialize vectors that store the outcome of each realization
    R_HBq1=zeros(N_average,1);R_HBq2=zeros(N_average,1);
    FDP_HBq1=zeros(N_average,1);FDP_HBq2=zeros(N_average,1);
    beta_HBq1=zeros(N_average,1);beta_HBq2=zeros(N_average,1);
    
    FDP_L11=zeros(N_average,1);FDP_L12=zeros(N_average,1);
    beta_L11=zeros(N_average,1);beta_L12=zeros(N_average,1);
    R_L11=zeros(N_average,1); R_L12=zeros(N_average,1);
    
    for ii=1:N_average
        %% -----------  Generating observations -----------
        y_tild= Whitening_matrix*mean/c+ randn(p,1);
        %% -----------    Benjamini-Hochberg Step-up procedure   -----------
        [ FDP_HBq1(ii),beta_HBq1(ii),R_HBq1(ii) ] = Benjamini_Hochberg( pseudo_inverse*y_tild,k,q1,p );
        [ FDP_HBq2(ii),beta_HBq2(ii),R_HBq2(ii) ] = Benjamini_Hochberg( pseudo_inverse*y_tild,k,q2,p );
        %% -----------    L_1 procedure   -----------
        [ FDP_L11(ii),beta_L11(ii),R_L11(ii) ] = SLOPE( y_tild,Whitening_matrix/c,lambda_q1,k,p );
        [ FDP_L12(ii),beta_L12(ii),R_L12(ii) ] = SLOPE( y_tild,Whitening_matrix/c,lambda_q2,k,p );
    end
    FDP_HBq_std_dev1(j)=sqrt(var(FDP_HBq1));
    FDP_HBq_std_dev2(j)=sqrt(var(FDP_HBq2));
    FDR_HBq_aver1(j)=mean2(FDP_HBq1);
    FDR_HBq_aver2(j)=mean2(FDP_HBq2);
    beta_HBq_aver1(j)=mean2(beta_HBq1);
    beta_HBq_aver2(j)=mean2(beta_HBq2);
    
    FDP_L1_std_dev1(j)=sqrt(var(FDP_L11));
    FDP_L1_std_dev2(j)=sqrt(var(FDP_L12));
    FDR_L1_aver1(j)=mean2(FDP_L11);
    FDR_L1_aver2(j)=mean2(FDP_L12);
    beta_L1_aver1(j)=mean2(beta_L11);
    beta_L1_aver2(j)=mean2(beta_L12);
    
end
%% Plot results
figure
plot(k_axis,FDR_L1_aver1,'ro-')
hold on
plot(k_axis,FDR_HBq_aver1,'r^-')
plot(k_axis,FDR_L1_aver2,'bo--')
plot(k_axis,FDR_HBq_aver2,'b^--')
xlabel('k')
ylabel('FDR')
legend('q=0.1 Slope','q=0.1 Marginal','q=0.05  Slope','q=0.05, Marginal')
grid on

figure
plot(k_axis,FDP_L1_std_dev1,'ro-')
hold on
plot(k_axis,FDP_HBq_std_dev1,'r^-')
plot(k_axis,FDP_L1_std_dev2,'bo--')
plot(k_axis,FDP_HBq_std_dev2,'b^--')
xlabel('k')
ylabel('Standard deviation of FDP')
legend('q=0.1 Slope','q=0.1 Marginal','q=0.05  Slope','q=0.05, Marginal')
grid on

figure
plot(k_axis(1:end),beta_L1_aver1(1:end),'ro-')
hold on
plot(k_axis(1:end),beta_HBq_aver1(1:end),'r^-')
plot(k_axis(1:end),beta_L1_aver2(1:end),'bo--')
plot(k_axis(1:end),beta_HBq_aver2(1:end),'b^--')
xlabel('k')
ylabel('Power')
legend('q=0.1 Slope','q=0.1 Marginal','q=0.05  Slope','q=0.05, Marginal')
grid on
