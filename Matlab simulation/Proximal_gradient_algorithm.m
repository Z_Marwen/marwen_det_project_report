function [b] = Proximal_gradient_algorithm( y,X,lambda,p )
iteration_max=10;
b=ones(p,1);
t=1/(norm(X,'fro')^2);
for k=0:iteration_max
    b=b-t*X'*(X*b-y);
    % solving prox problem
    [y_abs_sorted,index]=sort(abs(y),'descend');
    Permute=zeros(p,p);
    for i=1:p
        Permute(i,index(i))=1;
    end
    % now we have y_abs_sorted=  Permute * y_abs; y_abs=Permute'*y_abs_sorted;
   [ b ] = Stack_based_algorithm( y_abs_sorted,lambda );
%     b= solve_quadratic( y_abs_sorted,lambda );
    b= Permute'*b; % optimal solution before permutation
    b= sign(y).* b; % restoring the sign of x
end
end

