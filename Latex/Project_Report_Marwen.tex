\documentclass[11pt,journal,a4paper,onecolumn,draftclsnofoot]{IEEEtran}
\usepackage{mathtools}
\usepackage{amsmath,amssymb,amsthm,mathrsfs,amsfonts,dsfont} 
\usepackage{color}
\usepackage{ulem}
\usepackage{amsmath,amssymb,graphicx,array}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\usepackage{listings}
\usepackage{setspace}
\newcommand\underrel[2]{\mathrel{\mathop{#2}\limits_{#1}}}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{graphics,
           psfrag,
           epsfig,
           amsthm,
           cite,
           amssymb,
           url,
           dsfont,
           subfigure,
           algorithm,
           algorithmic,
           balance,
           enumerate,
           color,
           setspace,
           epstopdf
}
\DeclareMathOperator*{\argmax}{\arg\!\max}

\makeatletter
\newcommand{\rmnum}[1]{\romannumeral #1}
\newcommand{\Rmnum}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{titlepage}
	\centering
	%\includegraphics[width=0.15\textwidth]{example-image-1x1}\par\vspace{1cm}
	{\scshape\LARGE Univeristy of California, Irvine \par}
	\vspace{1cm}
	{\scshape\LARGE EECS 251A Detection \par}
	\vspace{1.5cm}
	{\huge\LARGE  Project report \par}
	\vspace{2cm}
	{\LARGE Author: Marwen \textsc{Zorgui} \par}
	\vfill
	\par
	\LARGE Professor: ~Anima \textsc{Anandkumar}
	\vfill

% Bottom of the page
	{\large December 2015\par}
\end{titlepage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\normalsize
\newpage
\begin{center}
\large
\bf Paper: ``Controlling the false discovery rate: a practical and
powerful approach to multiple testing'', Yoav Benjamini and Yosef Hochberg \cite{benjamini1995controlling}.
\end{center}

Multiple testing is ubiquitous across many scientific areas. Interpretating data and drawing inference with sound statistical rigor is usually subtle and challenging, especially when simultaneously dealing with increasing number of tests. Old techniques dealing with the problem of multiple testing usually suffer from low power performance. The seminal work in \cite{benjamini1995controlling} developed a novel approach dealing with this problem. The approach is based on the idea that in decision making, it is sometimes (if not in most cases) acceptable and preferable to allow for some wrong decisons as long as  the proportion of such wrong decisions is relatively low, as this would intuitively allow us to detect/decide in favor of many involved factors. In the sequel, we explain the inherent problem related to multiple testing, the classical approach and the technical novelty of controlling the false discovery rate.
\subsection{Multiple testing problem}
When dealing with a hypothesis test problem, two types of erros can be distinguished. Type 1 error corresponds to a decision if favor of the alternative hypothesis when the null hypothesis is true. This is usually referred to also as false alarm event, characterized by its probability
$
\alpha= \text{P(Type 1 Error)}
$. Deciding the null hypothesis when the alternative is true is know as a type 2 error. Such error is termed mis-detection error and is characterized by its probability $\beta=\text{P(Type 2 Error)}$. Usually, one is interested in maximizing the \textit{power of the test}, given by $1-\beta$, subject to a given false alarm rate $\alpha$. For completeness, we mention that the terms \textit{rejecting the null hypothesis} and \textit{making a discovery} are used interchangeably to refer to the situation in which we reject the null and accept the alternative hypothesis.
 
Now, we consider a case where we deal with simultaneous $m$ hypotheses. If each test is carried out subject to the same false alarm rate $\alpha$, then the probability of making a false discovery will increase as $m$ increases. For instance, assuming indepence of the tests, we have
\begin{align*}
\text{P( Type 1 error in m tests)}=\text{P( making at least 1 false positive in m tests)}=1-(1-\alpha)^m.
\end{align*}

Such obervarion calls for the need of controlling the type 1 error for multiple tests. In the litterature, the problem goes by different equivalent terminologies as the multiple comparisons, multiplicty or multiple testing problem. 
\subsection{FWER Error Control}
For a given procedure, one can classifiy the result in the following table:
\begin{center}
\begin{tabular}{ |p{3.8cm}||p{2.5cm}|p{2.5cm}|p{1.5cm}|  }
%
 \hline
  & Declared Non-Significant & Declared Significant& Total\\
 \hline
 \text{True null hypotheses}   & U    &V&   $m_0$\\
 \hline
 \text{Non-true null hypotheses}&   T  & S   &$m-m_0$\\
 \hline\label{table1}
 \text{Total} &$m$-R & R&  $m$\\
 \hline 
\end{tabular}
\end{center}

The classical and common control approach focused on controlling \textit{the family-wise error rate} (FWER). The FWER corresponds to the type 1 error, i.e., $\text{FWER}= \text{P} (V\geq 1$). One famous approach for controlling the FWER is the Bonferroni approach which consists in using a per-comparison significance (false-alarm) level $\frac{\alpha}{m}$. While such approach guarantees control of the FWER, the resulting theshold usually suffers from low power. This tends practically to wipe out evidence of the most interesting effects. We note that different types of control are studied, namely control in the weak sense and control in the strong sense. We ommit such details as our goal is to cover the general idea in this report.

\subsection{FDR Control}
FWER control is too stringent and results in low power. As argued in \cite{benjamini1995controlling}, in many cases, one can tolerate a certain number of false positives (discoveries). In these cases, a more relevant metric to control is rather \textbf{the false discovery rate} (FDR), which is \textit{the proportion of false discoveries among all discoveries}. The FDR is defined as $\text{FDR}= \mathbb{E}( \frac{V}{R})$, where $V$ and $R$ are defined as in the table above. A further reason for the control of the FDR is that usually model selection occurs at different stages. Variables initially selected at a certain FDR level would be the subject of a follow-up selection test, which would decrease further the probablity of a type 1 error. 

In addition to identifying the FDR as the metric of relevance in most settings, \cite[Theorem 1]{benjamini1995controlling} introduced a procedure, known as the $\text{BHq}$-procedure, that controls the FDR at a desired rate $q$. Equivalently, it guarantees that $\text{FDR} \leq q$. The BHq procedure is adaptive in the sense it is data-dependent. As shown in \cite[Theorem 2]{benjamini1995controlling}, the BHq procedure can be interpreted as choosing carefully a new per-comparison significance level $\alpha^*$ based on the observations. Such choice of $\alpha^*$ maximizes the power and limits the proportion of false discoveries, which is the desired behavior.

As shown by numerical examples in \cite{benjamini1995controlling}, the FDR procedure provides a gain in power that can be substantial, when compared to traditional methods controlling the FWER. 

Finally, we note that an exact difference between the power of the FDR and that of the FWER procedures at the same level is not exactly known. Investigating this difference would be of importance. Moreover, it is important to mention that the BHq procedure controls the FDR under the assumption of independence of noise samples with no guarantee in the other case. That constitutes a limitation to the BHq procedure as many practical scenarios exhibit some dependencies. On the other hand, the FDR approach enjoys better power performance by relying on the observed data (specfically $p$-values). This same aspect can be somehow a limiting factor to the overall approach. For instance, \textit{p-values are not reproducible}. This means that the p-values may differ greatly if the experiment is repeated. Recall that p-values are uniformly distributed under the null hypotheses. Thereofere, analyzing the sensitvity of the BHq approach to the observed p-values is of paramount importance to \textit{avoid making fool of ourselves} when making decisions. We note that some articles are investigating such limitations along these lines, we refer for example to \cite{colquhoun2014investigation}. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\begin{center}
\large
\bf Paper: ``Statistical estimation and testing via the sorted L1 norm
'', Malgorzata Bogdan, Ewout van den Berg, Weijie Su and Emmanuel Candes\cite{bogdan2013statistical}.
\end{center}	

The paper \cite{bogdan2013statistical} studies the classical problem of statistical estimation/testing of a linear model described by  
\begin{align*}
y= X \beta + z.
\end{align*}
The interest in the linear model stems from the ubiquity and importance of such models across many areas such as biology and engineering. More specifically, the paper adresses scenarios in which, due to some a-priori knowledge, we may expect only a small fraction of the many involved variables to be associated with the observation/response. Equivalently, the vector of unknowns $\beta$ is expected to contain only few non-zero entries, i.e, $\beta$ is sparse. Including such information into the problem formulation, we are interested in solving
\begin{align*} 
\min_{\beta} \lVert y- X \beta \lVert_2^2 + \lambda \lVert  \beta \lVert_0,
\end{align*}
where the regulization term $\lVert  \beta \lVert_0$ denotes the number of nonzero entries in $\beta$. Unfortunately, such optimization problem is not tractable as the number of unknowns $p$ increases. Several alternatives have been proposed in the litterature. One popular approach  consists in substituting the nonconvex term $\lVert  \beta \lVert_0$ by an $l_1$ norm convex approximation, which results in the \textbf{lasso} optimization problem
\begin{align*} 
\min_{\beta} \lVert y- X \beta \lVert_2^2 + \lambda \lVert  \beta \lVert_1.
\end{align*}
While the lasso is computationnally and practically appealing, it suffers form a \textit{dichotomy} limitation.
On the one hand, if the selected value of $\lambda$ is too small, then the lasso would select very many irrelevant variables (thus compromising its predicting performance). On the other hand, a large value of $\lambda$ would yield little power as well as a large bias. In the spirit of overcoming/reducing such limitations, the authors in \cite{bogdan2013statistical} introduce a novel method for sparse regression and variable selection. \cite{bogdan2013statistical}  suggests estimating the regression coefficients by a new estimator, called \textbf{the ordered lasso}, which corresponds to the following optimization problem
\begin{align*} 
\min_{\beta} \lVert y- X \beta \lVert_2^2 + \sum_{i=1}^{p} \lambda_i   \beta_{(i)},
\end{align*}
where the scalars $\lambda_i$'s satisfy $\lambda_1 \geq \lambda_2 \geq \ldots \lambda_p \geq 0$ and $\lvert \beta\rvert_{(1)} \geq \lvert \beta\rvert_{(2)} \geq \ldots \lvert \beta\rvert_{(p)} $ is the order statistic of the magnitudes of $\beta$. The above optimization problem is called \textbf{SLOPE}: Sorted L-One Penalized estimation.  We note that SLOPE is adaptive in the sense that the penalization (given by the nonincreasing sequence of $\lambda_i$'s) is adaptive to the number of nonzero coefficients. The cost of introducing a new variable is decreasing, in sharp contrast to the lasso approach in which the penalization term is uniform. Intuitively, this allows the algorithm to introduce more nonzero components, which may lead to a better detection/estimation performance.

More interestingly, the authors in \cite{bogdan2013statistical} made the link between the presented approach and the BHq approach \cite{benjamini1995controlling}. For instance, it is proven that the ordered lasso can be used to guarantee a certain desired level of false discovery rate by setting $\lambda_i$ to be the same coefficients used in the BHq procedure, solving the SLOPE and then rejecting all hypotheses with nonzero $\beta_i$. 

The SLOPE program is convex. Nevertheless, the authors in \cite{bogdan2013statistical} developed a fast iterative algorithm dedicated to efficiently solving the SLOPE.  This efficient computational aspect makes the ordered lasso an even more appealing solution to the problem of model selection.

It is worth mentionning that even though the ordered lasso is proved to control the FDR rate in the orthogonal design (in case the columns of $X$ are orthogonal), it is argued and highlighted numerically that the ordered lasso present significant improvement over the BHq procedure in the case of non-orthogonal design (in case the columns of $X$ are not strongly correlated). Our simulation results (reproducing the resuts of section 5.1.2 in \cite{bogdan2013statistical}) confirm the adequacy of the ordered-lasso in the non-orthogonal desgin as well as the computational appeal of the ordered lasso exhibits.

In short, the ordered lasso provides an elegant formulation and solution to the problem of estimation/testing of a linear model with sparsity constraint. This motivates future research about the true behavior of the ordered lasso in the case of non-orthogonal design. The choice of the sequence of $\lambda$ is also worth investigating. One possibe direction is to investigate whether a data-dependent choice would improve the performance as in the case of the BHq procedure. 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\begin{center}
\large
\bf Paper: ``Controlling the False Discovery Rate via Knockoffs'', Rina Foygel Barber and Emmanuel J. Candes\cite{barber2014controlling}.
\end{center}

The work in \cite{barber2014controlling} belongs to the rich litterature addressing the problem of model selection. In various settings, we deal with a high number of explanatory variables and a response/observation variables. The goal is to determine the explanatory variables associated with the response.
An imporatnt metric in this context is the false discovery rate (FDR) \cite{benjamini1995controlling} which calls for controlling the expected
fraction of false discoveries. 
The model of interest in \cite{barber2014controlling} is the linear model expressed as follows
\begin{align*}
y= X \beta + z,
\end{align*}
where $X (n \times p)$ is the design matrix, $\beta$ contains the unknowns parameters and $z$ is Gaussian noise $n \sim \mathcal{N}(0, \sigma^2 I)$. 
The authors in \cite{barber2014controlling} introduce 
the knockoff filter, a variable selection method that controls the
FDR in linear models when the number of observations is greater than the number of
variables, i.e., $n >p$. The basic idea is to construct additionnal variables, called “knockoff variables”, which exhibit the same correlation structure as the original structure 
and allow control of the FDR. The knockoff filter achieves exact FDR control in finite sample settings in which $n > p$ , no matter the design matrix $X$, covariates, and the number of regression coefficients ( i.e., structure of $\beta$ ) and it is shown numerically to provide more power that existing methods. We recall that the BHq procedure in \cite{benjamini1995controlling} performs poorly in case of dependent signals. The sorted lasso in \cite{bogdan2013statistical}, while providing significant improvent in the case of non-orthogonal design, does not guarantee exact control of the FDR at a desired level. Moreover, the knockoff filter does not require knowledge of the noise variance $\sigma^2$, which is yet another appealing feature to the filter.

\subsection*{Knockoff construction}
We describe briefly the construction and the operation of the filter. 
\begin{enumerate}
\item	The first step is the construction the knockoff features. For each variable $X_j$ (a column of the design matrix), we construct a corresponding knockoff feature $\tilde{X}_j$ satisfying
\begin{align*}
\tilde{X}_j^{'} \tilde{X}_k &= \tilde{X}_j^{'} X_k, \forall j,k \\
\tilde{X}_j^{'} \tilde{X}_k &= X_j^{'} X_k, \forall j \neq k.
\end{align*}
Clearly, The knockoff features exhibit the same correlation structure as the original features and also the same correlation between distinct features. We note that the generation of the new features does not require additional data and can be computer-generated.

\item	The second step is to design a statistic $W_j$ for each variable $j=1, \ldots ,p$ such that large positive values represent a strong evidence that the corresponding variable $\beta_j$ is a true explanatory variable. To compute the statistic, the filter uses an existing variable selection method, which is chosen to be the Lasso. For each feature $\beta_j$, it computes the earliest time the feature appears in the Lasso model. 
\begin{align*}
Z_j= \text{sup} \{ \lambda: \hat{\beta}_j(\lambda)  \neq 0 \}. 
\end{align*}
Large values of the statistic  $Z_j$ provides evidence in favor of including the variable $X_j$. 
These statistics are computed for all original and knockoff features and that by running the Lasso on the larger matrix $[ X \hat{X}]$. Next, the filter computes the statistic $W_j, j=1,\ldots, j$ as
\begin{align*}
W_j= \text{max}(Z_j ,\  \hat{Z}_j ) \times  \left\lbrace
  \begin{array}{r@{}l}
    +1  \text{ if } Z_j > \hat{Z}_j \\
    -1  \text{ if } Z_j < \hat{Z}_j.
  \end{array}
  \right.
\end{align*}

\item We compute a data-dependent threshold $T$ as 
\begin{align*}
T= \text{ min } \left\{ t \in \mathcal{W}:
\frac{1+\lvert \left\{ j: W_j \le -t \right\} \rvert}
{\text{ max } \left( \lvert \left\{ j: W_j \geq t \right\} \rvert , 1 \right) } \le q
 \right\},
\end{align*}
where $\mathcal{W}$ is the set of nonzero $|W_j|$'s.

\item	Finally, the knockoff procedure selects the variables whose statistic $W_j$ exceeds $T$. Equivalently, we have
\begin{align*}
S= \left\{ j: W_j \geq T \right\}.
\end{align*}

\end{enumerate}

The main contribution of the paper \cite[Theorem 2]{mestre2006finite} states that the above procedure controls the FDR for any matrix $X$ (only requirement is for $X^{'}X$ to be invertible) and for any configuration of $\beta$.

In summary, the knockoff filter introduces a novel approach controlling the FDR for any design matrix and configuration of the unkonown parameters . Such behavior was not exhibited by earlier works. In that sense, the knockoff filter is expected to find considerable practical applications. Such construction raises further interesting questions to understand/enhance the performance of the filter. One question relates to whether the data threshold $T$ should depend on the data to achieve a good model selection capability. Besides, the knockoff filter runs the Lasso to contruct statistics for the features. One may investigate whether investigating the ordered Lasso \cite{bogdan2013statistical}may provide better results in terms of FDR control and/or power.








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage
\begin{center}
\large
\bf Paper: ``Finite sample size effect on minimum variance beamformers: Optimum diagonal loading factor for large arrays'',Mestre, Xavier and Lagunas, Miguel {\'A}ngel\cite{mestre2006finite}.
\end{center}

This paper studies the performance of the minimum variance distortionless response (MVDR) beamformer which is one of the most important schemes for spatial filtering. This kind of filters is widely used in radar, sonar and sensor array applications. As many applications in wireless communications or array signal processing, the received signal is composed of a desired signal and interference plus noise term. Consequently, the performance of the MVDR filter is directly affected by the covariance matrix of the noise plus interference term which, for simplicity, will considered as noise. Perfect knowledge of the noise covariance matrix allows us to design the optimal MVDR filter that maximizes the signal-to-noise ratio (SNR) at the output of the array. However, in practice, this assumption does not hold, thus the design of the optimal filter seems out of reach. For this reason, an estimate for the noise covariance matrix is of tremendous importance. Note that, an imprecise estimate of the noise covariance matrix leads to a severe performance degradation as proved in the paper, this is why the noise covariance matrix should be estimated adequately. One popular technique is the sample covariance matrix (SCM) which is known to give good performance when the number of snapshots $N$ is much greater than the signal dimension $M$. Note that the design of the MVDR filter suggests the use of the inverse of the noise covariance matrix, that is why one needs to ensure that covariance matrix is invertible which is not the case for SCM when $N<M$. To guarantee some robustness, diagonal loading (i.e., adding a constant to all the elements of the diagonal of the sample correlation matrix) has been used as a means to alleviate the finite sample size impairments and to ensure that that the matrix is invertible. The paper considers a novel asymptotic approximation when both the number of snapshots $N$ and the signal dimension $M$ increase without bound, while their quotient remains constant. This kind of approximations is very appropriate for applications with large arrays such as sonar and radioastronomy. Interestingly, this kind of approximations is also applicable in situations with a relatively low number of arrays, since the proposed approach assumes the number of arrays and the number of snapshots have the same order of magnitude, which is always the case in practice. The derivations in \cite{mestre2006finite} are based on random matrix theory, and the statistical analysis of observations of increasing dimensions known as the general statistical analysis or G-analysis introduced and developed by Girko in the 1990s \cite{girko2012theory}.



In the sequel, we describe the system model and state the main results.
\section*{Signal Model and MVDR Beamforming}
Let us consider a generic antenna array of $M$ elements
working with a sample size of $N$ snapshots.
\begin{equation}
\mathbf{y}(n) = \beta s(n)\mathbf{s}_d + \mathbf{n}(n), \: \:  n=1 \cdots N,
\end{equation}
where $\beta$ indicates whether the desired signal is present ($\beta=1$) or not($\beta=0$), $s(n)$ is an unknown desired transmitted signal, $\mathbf{s}_d \in \mathbf{C}^{M\times 1}$ is the desired signal signature and $\mathbf{n}(n)\in \mathbf{C}^{M\times 1} $ contains the noise-plus-interference contribution. 
The beamformer weights is chosen to maximize the output SINR and is denoted as $\mathbf{w}_{opt}$ defined as 
\begin{equation}
\mathbf{w}_{opt}
= \argmax_{\mathbf{w}} \text{SINR}(\mathbf{w}), 
\:\: s.t. \: \mathbf{w}^H\mathbf{s}_d=1.
\end{equation}
where $\text{SINR}(\mathbf{w})=\frac{|\mathbf{w}^H\mathbf{s}_d|^2}{\mathbf{w}^H\mathbf{R}_N\mathbf{w}}$ and $\mathbf{R}_N$ is the noise covariance matrix. Using simple Lagrangian techniques, it is easy to prove that the problem accepts a closed-form solution given by
\begin{equation}
\mathbf{w}_{opt}=\frac{1}{\mathbf{s}_d \mathbf{R}_N^{-1}\mathbf{s}_d}\mathbf{R}_N^{-1} \mathbf{s}_d.
\end{equation}
Defining the total spatial correlation matrix of the observations as  
\begin{equation}
\mathbf{R} = \mathbb{E}\left[\mathbf{y}(n)\mathbf{y}^H(n)\right]=\beta \mathbf{s}_d\mathbf{s}_d^H + \mathbf{R}_N.
\end{equation}
Then, using the matrix inversion lemma, the optimal beamforming vector is given by 
\begin{equation}
\mathbf{w}_{opt} = \gamma \mathbf{R}^{-1} \mathbf{s}_d,
\end{equation}
where $\gamma = \frac{1+\beta\mathbf{s}_d \mathbf{R}_N^{-1}\mathbf{s}_d }{\mathbf{s}_d \mathbf{R}_N^{-1}\mathbf{s}_d}$ yielding an $\text{SINR} = \mathbf{s}_d \mathbf{R}_N^{-1}\mathbf{s}_d$. However, in practice the total correlation matrix is not perfectly known, thus the optimal beamformer is not known as well. For this reason, as estimate for it is needed, and the optimal beamformer naturally suggests the use of the vector $\hat{\mathbf{w}} = \hat{\mathbf{R}}^{-1}\mathbf{s}_d$, where $ \hat{\mathbf{R}}$ is the sample spatial correlation matrix
\begin{equation}
 \hat{\mathbf{R}} = \frac{1}{N} \sum_{n=1}^N \mathbf{y}(n)\mathbf{y}^H(n) = \frac{1}{N} \mathbf{Y}\mathbf{Y}^H.
\end{equation}
In some cases, the number of snapshots $N$ is for some reasons less than $M$, thus the sample correlation matrix is not invertible. Here comes the necessity to use a diagonal loading factor $\alpha$ in order to mitigate the finite-sample size effects,
\begin{equation}
\hat{\mathbf{w}} = \left[\hat{\mathbf{R}}+\alpha \mathbf{I}_M\right]^{-1}\mathbf{s}_d.
\end{equation}
\section*{Main Result}
Assuming that $\frac{M}{N}=c < \infty$, for any $\epsilon >0$, we have 
\begin{equation}\label{main}
\lim _{M,N \rightarrow \infty} \text{Prob} \left(|\text{SINR}-\overline{\text{SINR}}|> \epsilon\right) = 0,
\end{equation}
where $\overline{\text{SINR}} = \left(q(\alpha)-\beta\right)^{-1}$, 
$q(\alpha) = \frac{1}{1-c\xi}\frac{\mathbf{s}_d^H \left(\mathbf{R}+\eta \mathbf{I}_M\right)^{-1} \mathbf{R}\left(\mathbf{R}+\eta \mathbf{I}_M\right)^{-1} \mathbf{s}_d}{\left(\mathbf{s}_s^H\left(\mathbf{R}+\eta \mathbf{I}_M\right)^{-1} \mathbf{s}_s\right)^2}$ , $\xi= \frac{1}{M} \sum_{i=1}^M \left(\frac{\lambda_i}{\lambda_i+\eta}\right)^2$,\\ $\eta = \alpha\left(1+cb\right)$ and $b$ is the unique solution to the following equation
\begin{align*}
b = \frac{1}{M} \sum_{i=1}^M \frac{\lambda_i(1+cb)}{\lambda_i+\alpha(1+cb)},
\end{align*}
$\lambda_i$ are the ordered eigenvalues of $\mathbf{R}$.
Having the result in (\ref{main}), one can maximize $\overline{\text{SINR}}$ in terms of $\alpha$. However, as shown in the equation, $\overline{\text{SINR}}$ depends on $\mathbf{R}$ which is unknown in practice. This lead us to the next section which is the consistent estimation of the optimum loading factor using G-Estimation.
\section*{Consistent estimation of the optimum loading factor using General Statistical Analysis (G-Estimation)}
This new statistical analysis introduced by Girko builds on random matrix theory and provides a general framework for deriving estimators
that are consistent even when the observation dimension
increases at the same rate as the number of observations. Let's start by defining the two following functions 
\begin{align}
\mathcal{M}_{\mathbf{R}}(x) &= \frac{1}{M} \text{tr} \left[\left(\mathbf{I}_M+x\mathbf{R}\right)^{-1}\right], \: x \in \mathbb{R}^+ \\
\mathcal{T}_{\mathbf{R}}(x) & = \mathbf{s}_d^H \left(\mathbf{I}_M+x\mathbf{R}\right)^{-1} \mathbf{s}_d
\end{align}
In this context, the main objective of G-analysis is to find estimators
of Stieltjes transforms $\mathcal{M}_{\mathbf{R}}(x)$ and $\mathcal{T}_{\mathbf{R}}(x)$ that are consistent
when both $M$ and $N$ go to infinity at the same rate. Since
we are concentrating on Stieltjes transforms of the correlation
matrix $\mathbf{R}$ , the problem is equivalent to finding a transformation
of the sample correlation matrix $\hat{\mathbf{R}}$ that tends to the quantities
in (10) when $M,N \rightarrow \infty$. Interestingly, Girko proved that such an estimator exists and is given by 
\begin{align*}
\hat{\mathcal{M}}_{\mathbf{R}}(x) &= \frac{1}{M} \text{tr} \left[\left(\mathbf{I}_M+\theta(x)\hat{\mathbf{R}}\right)^{-1}\right], \: x \in \mathbb{R}^+ \\
\hat{\mathcal{T}}_{\mathbf{R}}(x) & = \mathbf{s}_d^H \left(\mathbf{I}_M+\theta(x)\hat{\mathbf{R}}\right)^{-1} \mathbf{s}_d
\end{align*}
where $\theta(x)$ denotes the positive solution to the following equation
\begin{align*}
\theta(x)\left[1-c+\frac{c}{M}\text{tr} \left[\left(\mathbf{I}_M+\theta(x)\hat{\mathbf{R}}\right)^{-1}\right]\right] = x, x>0
\end{align*}
Exploiting these tools, one can finally get to an expression for the consistent estimation (as $M,N \rightarrow \infty$) of the asymptotic optimum loading factor $\alpha$. The final expression is available in the original paper \cite{mestre2006finite}.
While the studied paper allows the study of the performance of the MVDR filter based on the  estimate given by the SCM, it is worth noticing that this kind of estimation may not be accurate in situations where the noise presents some \textit{non regularity} such in the case of impulsive noise or in the presence of outliers that may arise in many radar applications.
An alternative to the use of the SCM is represented by the class of robust scatter estimators which can be of much interest in such applications.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\begin{center}
\large
\bf  Simulation results
\end{center}
In this section, we provide results of our simulation. The simulations hilghlight the improvement in power provided by the ordered LASSO over the BHq procedure. We implement the fast algorithm developed in \cite{bogdan2013statistical} to efficiently solve the proximity operation optimization. For comparison, we implement as well a usual quadratic program solver (by calling the function \textit{quadrprog} built in Matlab). We observe numerically the significant savings in computation time offered by the stack-based algorithm in \cite{bogdan2013statistical}. While we do not provide numerical results due to space limitations, we note that to assess such gain with our code, on should simply uncomment the instruction $ 
b= solve\_quadratic( y\_abs\_sorted,lambda )$ in the code source of the function\\ 
$[b] = Proximal\_gradient\_algorithm( y,X,lambda,p )$.

To generate the figures of \cite[Figure 8]{bogdan2013statistical}, run the script $\textit{Main}$.

To generate the figures of \cite[Figure 9]{bogdan2013statistical}, run the script $\textit{Main1}$.

We store the result of running the script \text{Main} in the file \textit{data}. The file \textit{data} can be loaded into Matlab and then we can run the part of the code generating the figures (at the end of the script \text{Main} ). Similarly, the file $data1$ contains the result of running the script $Main1$ and can be used to generate the corresponding figures.

We present below the output of running the scripts $\textit{Main}$  and $\textit{Main1}$ respectively.
\begin{figure}[b!]
\includegraphics[width=1\linewidth]{Figures/FDR.eps}
\caption{FDR}
\end{figure}

\begin{figure}
\includegraphics[width=1\linewidth]{Figures/Standard_deviation.eps}
\caption{Standard Deviation of FDP}
\end{figure}

\begin{figure}
\includegraphics[width=1\linewidth]{Figures/Power.eps}
\caption{Power of both procedures}
\end{figure}

\begin{figure}
\includegraphics[width=1\linewidth]{Figures/Observed_FDP_marginal.eps}
\caption{Observed FDP marginal}
\end{figure}

\begin{figure}
\includegraphics[width=1\linewidth]{Figures/Observed_TPP_marginal.eps}
\caption{Observed TPP marginal}
\end{figure}

\begin{figure}
\includegraphics[width=1\linewidth]{Figures/Observed_FDP_SLOPE.eps}
\caption{Observed FDP SLOPE}
\end{figure}

\begin{figure}
\includegraphics[width=1\linewidth]{Figures/Observed_TPP_SLOPE.eps}
\caption{Observed TPP SLOPE}
\end{figure}
 
\newpage
\bibliographystyle{IEEEtran}
\bibliography{biblio}
\end{document}
