%% Matlab code to generate Figure 9 of sorted Lasso paper
% Author: Marwen Zorgui
clear
close all
p=1000;
n=p;
rho=0.5 ;
Sigma=rho*ones(n,n)+(1-rho)*eye(n,n);
Whitening_matrix=Sigma^(-1/2);
mu=sqrt(2*log(p));
q1=0.1;
N_average=500;
c=norm(Whitening_matrix(:,1));
pseudo_inverse=pinv(Whitening_matrix);
% Ordered lasso coefficients
lambda_q1=zeros(n,1);
for i=1:n
    lambda_q1(i)= norminv(1-q1*i /(2*p),0,1);
end
%% Main code
k=50;
mean=zeros(p,1);
mean(1:k)=mu;
% Initialize vectors that store the outcome of each realization
R_HBq1=zeros(N_average,1);
FDP_HBq1=zeros(N_average,1);
beta_HBq1=zeros(N_average,1);

FDP_L11=zeros(N_average,1);
beta_L11=zeros(N_average,1);
R_L11=zeros(N_average,1);

parfor ii=1:N_average
    ii
    %% -----------  Generating observations -----------
    y_tild= Whitening_matrix*mean/c+ randn(p,1);
    %% -----------    Benjamini-Hochberg Step-up procedure   -----------
    [ FDP_HBq1(ii),beta_HBq1(ii),R_HBq1(ii) ] = Benjamini_Hochberg( pseudo_inverse*y_tild,k,q1,p );
    %% -----------    L_1 procedure   -----------
    [ FDP_L11(ii),beta_L11(ii),R_L11(ii) ] = SLOPE( y_tild,Whitening_matrix/c,lambda_q1,k,p );
end

%% Plot results
figure
histogram(FDP_HBq1,50)
xlim([0 1]);
xlabel('FDP')
ylabel('Frequency')
title('Observed FDP for marginal test')
figure
histogram(FDP_L11)
xlim([0 1]);
xlabel('FDP')
ylabel('Frequency')
title('Observed FDP for SLOPE')

figure
histogram(beta_HBq1,50)
xlim([0 1]);
xlabel('FDP')
ylabel('Frequency')
title('Observed TPP for marginal test')
figure
histogram(beta_L11)
xlim([0 1]);
xlabel('FDP')
ylabel('Frequency')
title('Observed TPP for SLOPE')
