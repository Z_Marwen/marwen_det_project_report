function [ FDR_L1,beta_L1 ,R_l1 ] = SLOPE( y,X,lambda,k,p )
[b] = Proximal_gradient_algorithm( y,X,lambda,p );
x=abs(b);
Rejected_hypo_L1=find(x > 1e-5); % Rejected hypotheses
R_l1=length(Rejected_hypo_L1); % number of rejections
V_L1=length(find(Rejected_hypo_L1>k)); % number of false rejections;
FDR_L1=V_L1/(R_l1+(R_l1==0) ); % the denumerator is 1 only if R_HBq=0 !
beta_L1=length(find(Rejected_hypo_L1<=k))/k;
end

