function [ x ] = Stack_based_algorithm( y,lambda )
n=length(y);
i=zeros(n,1);
j=zeros(n,1);
s=zeros(n,1);
w=zeros(n,1);
x=zeros(n,1);
%% Find optimal group levels
t=0;
for k=1:n
    t=t+1;
    i(t)=k;
    j(t)=k;
    s(t)=y(k)-lambda(k);
    w(t)=max(s(t),0);
    while (t>1) && (w(t-1)<=w(t))
        i(t-1)=i(t-1);
        j(t-1)=k;
        s(t-1)=s(t-1)+s(t);
        w(t-1)=max(  s(t-1)/(1+k-i(t-1)) ,0);
        t=t-1;
    end
end
%% Set entries in x for each block
for l=1:t
    for k=i(l):j(l)
        x(k)=w(l);
    end
end

end

