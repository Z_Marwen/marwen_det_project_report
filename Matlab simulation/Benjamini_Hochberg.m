function [ FDR_HBq,beta_Hq,R_HBq ] = Benjamini_Hochberg( y_tild,k,q,p )
[~,index]=sort(abs(y_tild),'descend');
y_tild_sorted=y_tild(index);
i_su=0;
while abs(y_tild_sorted(i_su+1)) > norminv(1-q*(i_su+1)/(2*p),0,1)
    i_su=i_su+1;
end
R_HBq=i_su; % number of rejections= discoveries;
rejected_hypotheses=index(1:i_su);
V_HBq=length(find(rejected_hypotheses>k)); % number of false rejections;
FDR_HBq=V_HBq/(R_HBq+(R_HBq==0) ); % the denumerator is 1 only if R_HBq=0 !
beta_Hq = length(find(rejected_hypotheses<=k))/k;
end

